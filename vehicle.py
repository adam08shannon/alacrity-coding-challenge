class Vehicle:
    def __init__(self, id, make, price, yearOfManufacture):
        self.id = id
        self.make = make
        self.price = price
        self.yearOfManufacture = yearOfManufacture

    def getId(self):
        return self.id
    
    def getMake(self):
        return self.make
    
    def getPrice(self):
        return self.price
    
    def getYearOfManufacture(self):
        return self.yearOfManufacture