from car import Car

class Showroom:
    def __init__(self):
        self.vehicles = []
        self.index = 0
    
    def add(self, make, price, year):
        self.index += 1
        newVehicle = Car(self.index, make, price, year)
        self.vehicles.append(newVehicle)
        return newVehicle
    
    def getAveragePrice(self):
        if self.vehicleEmpty():
            return None
        total = 0

        for vehicle in self.vehicles:
            total += vehicle.getPrice()
        
        return total / len(self.vehicles)

    def getOldest(self):
        if self.vehicleEmpty():
            return None
        
        oldest = self.vehicles[0]
        for vehicle in self.vehicles:
            if oldest.getYearOfManufacture() > vehicle.getYearOfManufacture():
                oldest = vehicle
        
        return oldest

    def getMostExpensive(self):
        if self.vehicleEmpty():
            return None

        expensive = self.vehicles[0]
        for vehicle in self.vehicles:
            if expensive.getPrice() < vehicle.getPrice():
                expensive = vehicle
        
        return expensive

    def vehicleEmpty(self):
        return not self.vehicles
    