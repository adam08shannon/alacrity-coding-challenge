class Presenter:
    def __init__(self, showroom):
        self.showroom = showroom
    
    def getAveragePrice(self):
        average = self.showroom.getAveragePrice()
        if average:
            print("The average price of a car is : ", average)
        else:
            self.printEmpty()
    
    def getOldest(self):
        oldest = self.showroom.getOldest()
        if oldest:
            print("The oldest car is:")
            self.printVehicle(oldest)
        else:
            self.printEmpty()

    def getMostExpensive(self):
        mostExpensive = self.showroom.getMostExpensive()
        if mostExpensive:
            print("The most expensive car is: ")
            self.printVehicle(mostExpensive)
        else:
            self.printEmpty()

    def printVehicle(self, vehicle):
        print("ID: ", vehicle.getId())
        print("Make: ", vehicle.getMake())
        print("Price: ", vehicle.getPrice())
        print("Year of Manufacture: ", vehicle.getYearOfManufacture())

    def printEmpty(self):
        print("There are not vehicles in the showroom")

    def add(self):
        print("What is the vehicle make")
        make = input()
        price = self.getInt("What is the vehicle price")
        yearOfManufacture = self.getInt("What is the vehicles year of manufacture")
        self.showroom.add(make, price, yearOfManufacture)

    def getInt(self, question):
        val = "empty"

        while not val.isdigit():
            print(question)
            val = input()
        
        return int(val)
    
