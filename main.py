from showroom import Showroom
from presenter import Presenter

showroom = Showroom()

showroom.add("Aston Martin", 50, 2012)

showroom.add("BMW", 30, 2014)

showroom.add("Chevrolet", 20, 2013)

showroom.add("Datsun", 2, 2001)

presenter = Presenter(showroom)

val = 0

while val != 5:
    print("Make a selection from the below options")
    print("1. Get the average price")
    print("2. Get the oldest car")
    print("3. Get the most expensive car")
    print("4. Add a vehicle")
    print("5. Exit")

    val = input()

    if not val.isdigit():
        print("Please enter an integer value")
        continue
    
    val = int(val)

    if val == 1:
        presenter.getAveragePrice()
    elif val == 2:
        presenter.getOldest()
    elif val == 3:
        presenter.getMostExpensive()
    elif val == 4:
        presenter.add()
    elif val == 5:
        print("Goodybe")
    else:
        print("Please enter a value within the given range")
