from vehicle import Vehicle

class Car(Vehicle):
    def __init__(self, id, make, price, yearOfManufacture):
        super().__init__(id, make, price, yearOfManufacture)